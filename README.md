## ppe1_gsb
----
**Requirements**
*  PHP, Composer, Apache, MariaDB

**Instructions**
1. Clone project
2. Install composer dependencies `composer install`
3. Copy and configure your local .env file with your database informations `cp .env.example .env`
4. Generate an encryption key `php artisan key:generate`
5. Start the app `php artisan serve`