<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrat extends Model
{
    protected $table = 'contrat';
    protected $primaryKey = 'Id';
    public $timestamps = false;
}
