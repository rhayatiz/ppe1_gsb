<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contrat;
use App\Offre;


class OffreController extends Controller
{

    public function findAll()
    {
        $offres = Offre::all();
        return view('offres', compact('offres'));
    }


    public function find(int $id){
        $offre = Offre::find($id);
        return view('offre',   compact('offre'));
    }

    public function create(int $idPosition){
        $contrats = Contrat::all();
        return view('offre_create',compact('contrats', 'idPosition'));
    }

    public function postInfo(Request $request){

        $offre = new Offre();
        $offre->Description = $request->post('Description');
        $offre->ExperienceLvl = $request->post('ExperienceLvl');
        $offre->Datepublication = date("Y-m-d");
        $offre->DateDebut = $request->post("DateDebut");
        $offre->IdPosition = $request->post("IdPosition");
        $offre->IdContrat = $request->post("IdContrat");

        $offre->save();

        echo "<pre>";
        print_r($offre);
        // var_dump($request->post());
        echo "</pre>";
    }

}
