<?php

namespace App\Http\Controllers;

use App\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    public function find($id)
    {
        $position = Position::find($id);
    }

    public function findAll()
    {
        $positions = Position::all();
        return view('site', compact('positions'));
    }
}
