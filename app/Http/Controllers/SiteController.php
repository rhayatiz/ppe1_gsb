<?php

namespace App\Http\Controllers;

use App\Site;
use App\Position;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function find($id)
    {
        $site = Site::find($id);
    }

    public function getPositionsFromSite($id){
        $statement = Position::where('IdSite', $id);
        $positions = $statement->get();

        return view('site', compact('positions'));
    }

    public function findAll(){
        $sites = Site::all();
        return view('sites', compact('sites'));
    }
}
