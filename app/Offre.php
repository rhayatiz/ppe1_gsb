<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offre extends Model
{
    protected $table = 'offre';
    protected $primaryKey = 'Id';
    public $timestamps = false;
}
