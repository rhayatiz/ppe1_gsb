@include('header')


<div class="col col-lg-8 col-centered col-sm-12">


<form action="{{ URL::to('/postInfo') }}" method="POST">
@csrf

  <input type="hidden" name="IdPosition" value="{{ $idPosition }}">

  <div class="form-group">
    <label for="descriptionInput">Description de la position</label>
    <textarea class="form-control" name="Description" id="Description" rows="3"></textarea>
  </div>

  <div class="form-group">
    <label for="experienceLvlInput">Niveau d'éxpèrience</label>
    <input type="text" class="form-control" name="ExperienceLvl" id="experienceLvlInput" aria-describedby="emailHelp" placeholder="Niveau d'éxpèrience">
  </div>

  @php
    $date = date("Y-m-d",strtotime("yesterday"));
  @endphp

  <div class="form-group">
    <label for="dateDebutInput">Date de début</label>
    <input type="date" class="form-control" name="DateDebut" id="dateDebutInput" placeholder="Date de début" min="{{ $date }}">
  </div>

  <label for="IdContrat">Type du contrat</label>
  <select name="IdContrat" id="IdContrat" class="form-group">
      <option selected disabled>-- Choisir un type de contrat --</option>
      @foreach ($contrats as $contrat)
      <option value="{{ $contrat->Id }}">{{ $contrat->Libelle }}</option>
      @endforeach
</select>
  
  <button type="submit" class="btn btn-primary">Créer l'offre</button>

</form>

</div>


    </body>
</html>
