@include('header')

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Description</th>
      <th scope="col">ExperienceLvl</th>
      <th scope="col">DatePublication</th>
      <th scope="col">DateDebut</th>
      <th scope="col">IdPosition</th>
      <th scope="col">IdContrat</th>
    </tr>
  </thead>
  <tbody>
    <!-- Afficher les offres -->
    @foreach ($offres as $offre)
        <tr>
        <th>{{ $offre->Id }}</th>
        <td>{{ $offre->Description }}</td>
        <td>{{ $offre->ExperienceLvl }}</td>
        <td>{{ $offre->DatePublication }}</td>
        <td>{{ $offre->DateDebut }}</td>
        <td>{{ $offre->IdPosition }}</td>
        <td>{{ $offre->IdContrat }}</td>
        <td><a href="/offre/show/{{$offre->Id}}">Afficher positions</a></td>
        </tr>
    @endforeach
</table>

</div>
</div>

</body>
</html>
