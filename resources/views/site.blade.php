@include('header')
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Intitule</th>
      <th scope="col">Salaire</th>
      <th scope="col">IsOccupied</th>
    </tr>
  </thead>

  <tbody>
    <!-- Showing site's posisions -->
    @foreach ($positions as $position)
        <tr>
        <th>{{ $position->Id }}</th>
        <td>{{ $position->Intitule }}</td>
        <td>{{ $position->Salaire }}</td>
        @if ($position->IsOccupied)
        <td>poste occupé</td>
        @else
        <td>poste non occupé<br><a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/offre/create/{{$position->Id}}">Créer une offre</a>
      </td>
        @endif
        </tr>
    @endforeach
</table>

</div>
</div>
    </body>
</html>
