@include('header')

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nom</th>
      <th scope="col">Adresse</th>
      <th scope="col">Positions</th>
    </tr>
  </thead>
  <tbody>
    <!-- Showing site's posisions -->
    @foreach ($sites as $site)
        <tr>
        <th>{{ $site->Id }}</th>
        <td>{{ $site->Nom }}</td>
        <td>{{ $site->Adresse }}</td>
        <td><a href="/site/show/{{$site->Id}}">Afficher positions</a></td>
        </tr>
    @endforeach
</table>

</div>
</div>
    </body>
</html>
