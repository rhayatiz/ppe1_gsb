<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('accueil');
});

//login
Route::get('/login', function(){ 
    return view('connexion');
});

//group sites
Route::prefix('site')->group(function () {
    //show site's positions
    Route::get('show/{id}', 'SiteController@getPositionsFromSite')->where('id', '[0-9]+');

    //show all positions
    Route::get('show/all', 'PositionController@findAll');
});

//show all sites
Route::get('/sites', 'SiteController@findAll');

//create offre
Route::get('offre/create/{idPosition}', 'OffreController@create');
//afficher toutes les offres
Route::get('offre/show/all', 'OffreController@findAll');
//afficher offre
Route::get('offre/show/{id}', 'OffreController@find');



//recuperation données createoffer
Route::post('/postInfo', 'OffreController@postInfo');